const bcrypt = require('bcrypt');
const registerData = require('../data/registers');
const IP = require('ip');

const addUsers = async (req, res, next) => {
    try {
        const ipAddress = IP.address();
        console.log(ipAddress);
        bcrypt.hash(req.body.Password, 10, async (err, hash) => {
            if(err){
                return res.status(500).json({
                    error: err
                })
            }else{
                const data = {
                    Username: req.body.Username,
                    Email: req.body.Email,
                    Password: hash,
                    Fullname: req.body.Fullname,
                    Whatsapp: req.body.Whatsapp,
                    IsActive: 0,
                    CreateIP: ipAddress,
                    UpdateIP: ipAddress
                }
                const insert = await registerData.createReg(data);
                res.status(201).json({
                    message: 'Successfull Created!',
                    url: 'http://localhost:8080/api/login',
                    data: insert
                })
            }
        })

    } catch (error) {
        res.status(400).json(error.message); 
    }
}

module.exports = {
    addUsers
}