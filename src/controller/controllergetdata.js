const getData = async (req, res, next) => {
    res.json([
        {
          id: '001',
          name: 'Smith',
          email: 'smith@gmail.com',
        },
        {
          id: '002',
          name: 'Sam',
          email: 'sam@gmail.com',
        },
        {
          id: '003',
          name: 'lily',
          email: 'lily@gmail.com',
        },
        {
          id: '004',
          name: 'aziz',
          email: 'aziz@gmail.com',
        },
        {
          id: '005',
          name: 'ibnu',
          email: 'ibnu@gmail.com',
        },
        {
          id: '006',
          name: 'parhan',
          email: 'parhan@gmail.com',
        },
        {
          id: '007',
          name: 'tasya',
          email: 'tasya@gmail.com',
        },
        {
          id: '008',
          name: 'ana',
          email: 'ana@gmail.com',
        },
        {
          id: '009',
          name: 'indi',
          email: 'indi@gmail.com',
        },
        {
          id: '010',
          name: 'ona',
          email: 'ona@gmail.com',
        }
    ]);
}

module.exports = {
    getData
}