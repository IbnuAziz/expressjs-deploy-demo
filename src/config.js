const dotenv = require('dotenv');
const assert = require('assert');

dotenv.config();

const {SQL_USER, SQL_PASSWORD, SQL_SERVER, SQL_DATABASE} = process.env;

const sqlEncrypt = process.env.ENCRYPT === 'true';

assert(PORT, 'PORT is required');
assert(HOST, 'HOST is required');

module.exports = {
    sql: {
        server: SQL_SERVER,
        database: SQL_DATABASE,
        user: SQL_USER,
        password: SQL_PASSWORD,
        options: {
            encrypt: sqlEncrypt,
            useUTC: true,
            enableArithAbort: true,
            TrustServerCertificate:true
        }
    }
}