const utils = require('../utils');
const config = require('../../config');
const sql = require('mssql');

const createUsers = async (regData) => {
    try {
        let pool = await sql.connect(config.sql);
        const sqlQueries = await utils.loadSqlQueries('register');
        const insertReg = await pool.request()
                            .input('Username', sql.NVarChar(100), regData.Username)
                            .input('Password', sql.NVarChar(50), regData.Password)
                            .input('Fullname', sql.VarChar(100), regData.Fullname)
                            .input('Whatsapp', sql.VarChar(100), regData.Whatsapp)
                            .input('Email', sql.VarChar(100), regData.Email)
                            .input('IsActive', sql.VarChar(100), regData.IsActive)
                            .input('CreateID', sql.VarChar(100), regData.CreateID)
                            .input('CreateIP', sql.VarChar(100), regData.CreateIP)
                            .input('UpdateID', sql.VarChar(100), regData.UpdateID)
                            .input('UpdateIP', sql.VarChar(100), regData.UpdateIP)
                            .query(sqlQueries.regCreate);
        return insertReg.recordset;
    } catch (error) {
        return error.message
    }
}

// const getRegList = async () => {
//     try {
//         let pool = await sql.connect(config.sql);
//         const sqlQueries = await utils.loadSqlQueries('register');
//         const list = await pool.request().query(sqlQueries.regList);
//         return list.recordset;
//     } catch (error) {
//         return error.message;
//     }
// }

// const getRegListbyID = async (regID) => {
//     try {
//         let pool = await sql.connect(config.sql);
//         const sqlQueries = await utils.loadSqlQueries('register');
//         const singleReg = await pool.request()
//                             .input('regID', sql.Int, regID)
//                             .query(sqlQueries.regByID);
//         return singleReg.recordset;
//     } catch (error) {
//         return error.message;
//     }
// }

module.exports = {
    createUsers
}