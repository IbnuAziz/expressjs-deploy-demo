INSERT INTO [dbo].[users]
    (
      [Username]
      ,[Password]
      ,[Fullname]
      ,[Whatsapp]
      ,[Email]
      ,[IsActive]
      ,[CreateID]
      ,[CreateIP]
      ,[UpdateID]
      ,[UpdateIP]
    )
VALUES 
    (
        @Username,
        @Password,
        @Fullname,
        @Whatsapp,
        @Email,
        @IsActive
        @CreateID = [IDUser]
        @CreateIP
        @UpdateID = [IDUser]
        @UpdateIP
    )

SELECT SCOPE_IDENTITY() AS IDUser