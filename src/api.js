const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const serverless = require("serverless-http");

const app = express();
const router = express.Router();

const usersRoutes = require('./routes/registerRoutes');

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

router.get("/", (req, res) => {
  res.json({
    hello: "hi!"
  });
});

router.get('/add-users', usersRoutes.routes);

app.use(`/.netlify/functions/api`, router);

module.exports = app;
module.exports.handler = serverless(app);
