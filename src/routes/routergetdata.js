const express = require('express');
const controllerGetData = require('../controller/controllergetdata');
const router = express.Router();

router.get('/data-api', controllerGetData.getData);


module.exports = {
    router: router
}