const express = require('express');
const registerController = require('../controller/registerController');
const router = express.Router();

router.post('/add-users', registerController.addUsers);

module.exports = {
    routes: router
}